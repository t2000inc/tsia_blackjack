/*
  (c) Tech 2000. All Rights Reserved. http://t2000inc.com
*/

/*global window */
/*global ADL */
/*global Lumi */
/*global microAjax */
/*jslint white:true */

var LumiX = function()
  {
    this.openLocker = function (lid, uuid, iri)
      {
        if (window.ADL === 'undefined')
          {
            this.log('~~> XAPI API MISSING! <~~~');
          }

        if (uuid.length && iri.length)
          {
            this.course_id_uuid = uuid;
            this.course_uri = iri;
            this.breadcrumb(ADL.XAPIWrapper.hash(iri));

            $.ajax(
              {
                url: this.locker_store + lid,

                success: function(d)
                  {
                    Lumi.endpoint = d.uri.toString();
                    Lumi.user = d.username.toString();
                    Lumi.password = d.password.toString();

                    Lumi.change_config();

                    Lumi.log('L:' + Lumi.endpoint);
                    Lumi.log('u:' + Lumi.user);
                    Lumi.log('p:' + Lumi.password);

                    Lumi.initialized = true;

                    Lumi.log('Initialized.');
                  }
              });
          }
      };

    this.setLocker = function (ignored, b, c)
      {
        ignored = ignored || (b || c ) || null; // jslint fool

        this.log('WARNING: Method deprecated; use openLocker');

        return this.NOERROR_DEPRECATED;
      };

    this.setLearner = function (named, email)
      {
        var rc = this.ERROR.INVALID_PARAM;

        if (named.toString().length && email.toString().length)
          {
            this.student.named = named;
            this.student.inbox = 'mailto:' + email;

            this.actor = JSON.stringify(
              {
                'mbox':this.student.inbox,
                'name':this.student.named
              });

            rc = this.ERROR.NOERROR;
          }
        else
          {
            this.log("WARNING: Name and email required!");
          }

        return rc;
      };

    this.cue = function (verb, notes, extended)
      {
        var s = new ADL.XAPIStatement(
          this.user,
          verb,
          this.course_uri + this.course_id_uuid + '/'
          );

        s.actor = this.actor;

        s.object = {

            'id': this.course_uri + this.course_id_uuid + '/' + 'JSQbd' + '/',

            'definition':
              {
                'name':
                  {
                    'en-US': notes.toString()
                  },

                'description':
                  {
                    'en-US': notes.toString()
                  }
              },

            'objectType': 'Activity'
          };

        // Support extended args

        if (extended.hasOwnProperty('exarg'))
          {
            var nod = s.object.definition, en;

            nod.type = 'http://adlnet.gov/expapi/activities/cmi.interaction';

            nod.interactionType = 'performance';

            // Game stats

            nod.steps = [];

            for (en in extended.exarg)
              {
                nod.steps.push({'id':en,'description':{'en-US':extended.exarg[en]}});
              }
          }

        return this.send_statement(s);
      };

    this.addVerb = function (verb)
      {
        this.VERB[verb.toUpperCase()] = {
          'id' : 'http://learningconsole.com/verbs/' + verb.toLowerCase(),
          'display' : { 'en-US' : verb.toLowerCase() }
        };
      };

    this.addVerbEx = function (verb, obj)
      {
        var rc = this.ERROR.NOERROR;

        if (obj.hasOwnProperty('id'))
          {
            if (obj.hasOwnProperty('display'))
              {
                this.VERB[verb] = obj;
              }
            else
              {
                rc = this.ERROR.INVALID_PARAM;
              }
          }
        else
          {
            rc = this.ERROR.INVALID_PARAM;
          }

        return rc;
      };

    this.jot = function (property_bag)
      {
        var rc = this.ERROR.INVALID_PARAM;

        if (property_bag.hasOwnProperty('verb') && this.VERB.hasOwnProperty(property_bag.verb.toString().toUpperCase()))
          {
            var verb = property_bag.verb;

            delete property_bag.verb;

            rc = this.cue(this.VERB[verb.toString().toUpperCase()], '', { 'exarg':property_bag });
          }

        return rc;
      };

    // ~~~ end public api (intention) ~~~

    this.change_config = function()
      {
        ADL.XAPIWrapper.changeConfig(
          {
            'endpoint': this.endpoint,
            'user': this.user,
            'password': this.password,
            "name": this.student.named
          });

        return this.ERROR.NOERROR;
      };

    this.send_statement = function(s)
      {
        if (!this.initialized)
          {
            this.log('ERROR: Sending requires a locker!');

            return this.ERROR.NO_LOCKER_FOUND;
          }

        try
          {
            ADL.XAPIWrapper.sendStatement(s, function()
              {
                Lumi.log('Statement posted.');
              });
          }
        catch(e)
          {
            this.log('No statement sent; network connectivity???');
          }
      };

    this.breadcrumb = function (crumb)
      {
        if ((crumb !== undefined) && (null !== crumb) && (crumb.length))
          {
            this.last_registered_breadcrumb = encodeURIComponent(crumb);
          }
        else
          {
            this.last_registered_breadcrumb = this.hash(this.course_uri);
          }
      };

    this.hash = function (str)
      {
        return ADL.XAPIWrapper.hash(str);
      };

    this.log = function(msg)
      {
        if (this.logging_enabled)
          {
            console.log('LumiJS::' + msg);
          }
      };

    this.ERROR = {

      'NOERROR' : 0x0,
      'INIT_REQUIRED' : 0x01,
      'BAD_PARAM_FORMAT' : 0x02,
      'INVALID_PARAM' : 0x4,
      'NOERROR_DEPRECATED' : 0x0,
      'NO_LOCKER_FOUND' : 0x10
    };

    this.VERB = {

      'LAUNCHED' : {
         'id' : 'http://learningconsole.com/verbs/launched',
         'display' : { 'en-US' : 'launched' }
      }

    };

    this.initialized = false;

    this.endpoint = null;
    this.user = null;
    this.password = null;
    this.student = { 'named': null, 'inbox': null };

    this.course_id_uuid = '';
    this.course_uri = '';

    this.actor = null;

    this.locker_store = 'http://privacysphere.com/api/lrslink/';

    this.logging_enabled = false;
  };

var Lumi = new LumiX();