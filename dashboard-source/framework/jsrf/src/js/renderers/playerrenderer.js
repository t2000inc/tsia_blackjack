define([
  "generated/templates",
  "renderers/componentrenderer",
  "utils/colorutils",
  'vendor/lodash',
  "vendor/Modernizr"
], function (JST, ComponentRenderer, ColorUtils, _, Modernizr) {
  function PlayerRenderer() {
    ComponentRenderer.call(this);

    var self = this,
        base = {},
        Public,
        raw = self._raw,
        Protected,
        pro = self.pro,
        data = {},
        _bp = {};

    Public = {
      renderCore: function (playerData) {
        if(self.$core !== null){
          console.log("render core player data: ");
          console.log(playerData); 
          self.$core.html(JST.player_base(playerData));
        }
      },
      resizeCore: function (width, height) {

      },
      dispose: function () {
        base.dispose();
      },

    };

    Protected = {
  
    };

    raw._registerClassName("playerRenderer");
    raw._registerPublic(base, Public);
    raw._registerProtected(_bp, Protected);

  }

  return PlayerRenderer;
});
