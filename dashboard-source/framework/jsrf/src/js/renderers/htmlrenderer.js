define([
  "generated/templates",
  "renderers/componentrenderer",
  "utils/colorutils",
  'vendor/lodash',   
  "vendor/Modernizr"
], function (JST, ComponentRenderer, ColorUtils, _, Modernizr) {
  function HTMLRenderer() {
    ComponentRenderer.call(this);

    var self = this,
        base = {},
        Public,
        raw = self._raw,
        Protected,
        pro = self.pro,
        data = {},
        _bp = {};

    Public = {
      renderCore: function (htmlcontent) {
        if(self.$core !== null){
          console.log("render core html data: ");
          console.log(htmlcontent); 
          self.$core.html(JST.html_base(htmlcontent));
        }
      },
      resizeCore: function (width, height) {

      },
      dispose: function () {
        base.dispose();
      }
    };

    Protected = {
  
    };
   
    raw._registerClassName("htmlRenderer");
    raw._registerPublic(base, Public);
    raw._registerProtected(_bp, Protected);

  }

  return HTMLRenderer;
});
