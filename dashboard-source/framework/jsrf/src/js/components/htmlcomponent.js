define([
  "components/component",
  "renderers/htmlrenderer",
  "prop/properties",
  "utils/errorutils",
  "vendor/lodash"
], function (Component, HTMLRenderer, Properties, errorUtils, _) {
  /**
   * Creates a component which displays HTML code, useful for custom component needs
   * @class HTMLComponent
   * @augments {Component}
   */
  function HTMLComponent() {
    Component.apply(this, Array.prototype.slice.call(arguments));

    var self = this,
        base = {},
        Public,
        raw = self._raw,
        Protected,
        pro = self.pro,
        _bp = {};

    var htmlcontent = null;

    Public = {

      setContent: function (htmlcontent) {
        pro.renderer = new HTMLRenderer();
        pro.pb.htmlprop = { htmlcontent: htmlcontent };
        console.log("setContent HTML:");
        console.log(pro.pb.htmlprop);
      },

      updateContent: function (htmlcontent) {
        self.trigger("beforeComponentUpdate");
        pro.pb.htmlprop = { htmlcontent: htmlcontent };
        pro.renderer.renderCore(pro.pb.htmlprop);
        console.log("updateContent HTML:");
        console.log(pro.pb.htmlprop);
        self.trigger("componentUpdate");
      }

    },

      
    Protected = {
      init: function () {
        _bp.init();
      },

      createRenderer: function () {
        pro.renderer = new HTMLRenderer();
        pro.onRendererCreate();
      },
      resizeCore: function (width, height) {
        pro.renderer.resizeCore(width, height);
      },
      renderCore: function () {
        console.log("renderCore in HTMLComponent running...");
        console.log(pro.pb.htmlprop); 
        pro.renderer.renderCore(pro.pb.htmlprop);
      },
      addListeners: function () {
        _bp.addListeners();
        pro.pushListeners([

        ]);
      },
      defaultValues: {}
    };


    /**
     * This is the actual constructor of the object
     */
    var construct = function () {
      pro.pb = new Properties.HTMLComponentProperties();
    };

    raw._registerClassName("HTMLComponent");
    raw._registerPublic(base, Public);
    raw._registerProtected(_bp, Protected);

    construct();
  }

  return HTMLComponent;
});
