define([
  "components/component",
  "renderers/playerrenderer",
  "prop/properties",
  "utils/errorutils",
  "vendor/lodash"
], function (Component, PlayerRenderer, Properties, errorUtils, _) {
  /**
   * Creates a component which displays HTML code, useful for custom component needs
   * @class HTMLComponent
   * @augments {Component}
   */
  function PlayerComponent() {
    Component.apply(this, Array.prototype.slice.call(arguments));

    var self = this,
        base = {},
        Public,
        raw = self._raw,
        Protected,
        pro = self.pro,
        _bp = {};

    var playerData = null;

    Public = {

      setContent: function (playerData) {
        pro.renderer = new PlayerRenderer();
        pro.pb.player = playerData;
        console.log("setContent Data:");
        console.log(playerData);
      },

      updatePlayer: function (playerData) {
        self.trigger("beforeComponentUpdate");
        pro.pb.player = playerData;
        pro.renderer.renderCore(pro.pb.player);
        self.trigger("componentUpdate");
      },

      clearData: function () {
        pro.renderer.dispose();
      }

    },

      
    Protected = {
      init: function () {
        _bp.init();
      },
      createRenderer: function () {
        pro.renderer = new PlayerRenderer();
        pro.onRendererCreate();
      },
      resizeCore: function (width, height) {
        pro.renderer.resizeCore(width, height);
      },
      renderCore: function () {
        console.log("renderCore in PlayerComponent running...");
        console.log(pro.pb.player); 
        pro.renderer.renderCore(pro.pb.player);
      },
      addListeners: function () {
        _bp.addListeners();
        pro.pushListeners([

        ]);
      },
      defaultValues: {}
    };


    /**
     * This is the actual constructor of the object
     */
    var construct = function () {
      pro.pb = new Properties.PlayerComponentProperties();
    };

    raw._registerClassName("PlayerComponent");
    raw._registerPublic(base, Public);
    raw._registerProtected(_bp, Protected);

    construct();
  }

  return PlayerComponent;
});
