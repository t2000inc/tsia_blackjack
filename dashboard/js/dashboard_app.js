var usertotal = '';
var maxplayers = 4;

var baseurl = 'http://privacysphere.com/api/';
var topuserurl = baseurl + 'gamestatus/' + usertotal;
var bottomuserurl = baseurl + 'gamestatus/bottom/' + maxplayers;
var sumbalanceurl = baseurl + 'gamestatus/balance/ordered/';
var sumskillurl = baseurl + 'gamestatus/skill/ordered/';
var sumluckurl = baseurl + 'gamestatus/luck/ordered/';
var sumhandsurl = baseurl + 'gamestatus/hands/ordered/';

var comps = {};
var sums = {};

// execution delay time for refreshing data
var max = 10000;
var min = 5000;
var refreshTime = 10000//Math.round(Math.random() * (max - min) + min);
var refreshTime2 = 10000//Math.round(Math.random() * (max - min) + min);

var db1interval, db2interval;

var chart = {};
var chartlabels = [];
var chartskills = [];
var chartlucks = [];
var chartspeeds = [];
var charthands = [];

var toporbottom = topuserurl;
var topmode = "Top Player";

$.ajax({
    type: "GET",
    url: topuserurl,
    success: function (data) {
        rf.StandaloneDashboard(function (tdb) {
        	tdb.setTabbedDashboardTitle("&nbsp;");

        	var db = new Dashboard('db');
        	db.setDashboardTitle ("Leaderboard");

        	console.log(data);
            var i = 0;
			$.each(data, function(key, value){
				// set last updated time
		        var t = moment().format("MM/DD/YYYY @ hh:mm:ss a");

				// create arrays for overall performance chart data
				chartlabels.push(value.Name);
				chartskills.push(Number(value.Skill));
				chartlucks.push(Number(value.Luck));
				chartspeeds.push(Number(moment.duration(value.LastTime).asMinutes()));
				charthands.push(Number(value.Hands));

				playerData = { 
					'Name' : value.Name,
					'Balance' : value.Balance,
					'Skill' : value.Skill,
					'Luck' : value.Luck,
					'Hands' : value.Hands,
					'Personality' : value.Personality,
					'Level' : value.Level,
					'Badges' : '',
					'StartTime' : value.StartTime,
					'LastTime' : value.LastTime,
					'DecisionTime' : moment.duration(value.LastTime).humanize(),
					'Updated' : t
				 };

				if(i <= maxplayers-1){ 
					console.log("i = " + i);
					comps[i] = new PlayerComponent();
					comps[i].setDimensions(3, 3);
					comps[i].setCaption(topmode + " " + (i+1));
					comps[i].setContent(playerData);
				    db.addComponent(comps[i]);
				}

			    i++;
			});

			//console.log("db components: ");
			//console.log(db.components);

			// chart for user performance
	    	chart = new ChartComponent("sales");
			chart.setDimensions (12, 3);
			if(topmode == "Top Player"){
				tmpmode = "All Player";
			}else{
				tmpmode = "Bottom" + maxplayers + "Player";
			}
			chart.setCaption("Realtime Performance for "+tmpmode+"s | Updated: " + moment().format("MM/DD/YYYY @ hh:mm:ss a"));
			chart.setYAxis("Skill/Luck/Speed");
	    	chart.setLabels (chartlabels);
			chart.addSeries ("Skill", "Skill", chartskills);
			chart.addSeries ("Luck", "Luck", chartlucks);
			chart.addSeries ("Hands", "Hands", charthands);
			db.addComponent (chart);

			// automatically update the data at random intervals between 30 seconds and 1 minute
			db1interval = setInterval(function () {
				refreshLeaderBoard()
	    	}, refreshTime);

			tdb.addDashboardTab(db, {
        		title: 'Leaderboard',
        		active: true
		    });

			// Dashboard 2
			var sumbalance = $.getJSON(sumbalanceurl);
			var sumskills = $.getJSON(sumskillurl);
			var sumluck = $.getJSON(sumluckurl);
			var sumhands = $.getJSON(sumhandsurl);
			var db2 = new Dashboard('db2');
		    	db2.setDashboardTitle('&nbsp;');
		    var t2 = moment().format("MM/DD/YYYY @ hh:mm:ss a");

			$.when(sumbalance, sumskills, sumluck, sumhands).done(function(balance, skill, luck, hands){
				
				// prepare all of the data to be dumped into an object we can loop over
		    	var balancedata = _.values(JSON.parse(balance[2].responseText));
		    	var highbalance = _.last(balancedata);
		    	var lowbalance = _.first(balancedata);

		    	var skilldata = _.values(JSON.parse(skill[2].responseText));
		    	var highskill = _.last(skilldata);
		    	var lowskill = _.first(skilldata);

		    	var luckdata = _.values(JSON.parse(luck[2].responseText));
		    	var highluck = _.last(luckdata);
		    	var lowluck = _.first(luckdata);

		    	var handsdata = _.values(JSON.parse(hands[2].responseText));
		    	var highhands = _.last(handsdata);
		    	var lowhands = _.first(handsdata);


		    	console.log(_.values(highbalance));

		    	// dump into obect we can loop over
		    	var postgamedata = {
		    		hbalance : [
		    			'High Balance',
		    			'<i class=\"fa fa-money fa-5x fa-highcolor\"></i><h2>'+_.values(highbalance)[0]+'</h2><p>$'+_.values(highbalance)[1]+'</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		lbalance : [
		    			'Low Balance',
		    			'<i class=\"fa fa-money fa-5x fa-lowcolor\"></i><h2>'+_.values(lowbalance)[0]+'</h2><p>$'+_.values(lowbalance)[1]+'</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		hskill : [
		    			'High Skill',
		    			'<i class=\"fa fa-trophy fa-5x fa-highcolor\"></i><h2>'+_.values(highskill)[0]+'</h2><p>'+_.values(highskill)[1]+'/200</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		lskill : [
		    			'Low Skill',
		    			'<i class=\"fa fa-trophy fa-5x fa-lowcolor\"></i><h2>'+_.values(lowskill)[0]+'</h2><p>'+_.values(lowskill)[1]+'/200</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		hluck : [
		    			'High Luck',
		    			'<i class=\"fa fa-star fa-5x fa-highcolor\"></i><h2>'+_.values(highluck)[0]+'</h2><p>'+_.values(highluck)[1]+'/200</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		lluck : [
		    			'Low Luck',
		    			'<i class=\"fa fa-star fa-5x fa-lowcolor\"></i><h2>'+_.values(lowluck)[0]+'</h2><p>'+_.values(lowluck)[1]+'/200</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		hhands : [
		    			'High Hands',
		    			'<i class=\"fa fa-hand-stop-o fa-5x fa-highcolor\"></i><h2>'+_.values(highhands)[0]+'</h2><p>'+_.values(highhands)[1]+' HANDS</p><date>Last Updated: '+t2+'</date>'
		    		],
		    		lhands : [
		    			'Low Hands',
		    			'<i class=\"fa fa-hand-stop-o fa-5x fa-lowcolor\"></i><h2>'+_.values(lowhands)[0]+'</h2><p>'+_.values(lowhands)[1]+' HANDS</p><date>Last Updated: '+t2+'</date>'
		    		]
		    	}

		    	var y = 0;
		    	$.each(postgamedata, function(key, value){
		    		console.log('looping through post game modules:');
		    		console.log(value);
					sums[y] = new HTMLComponent();
					sums[y].setDimensions(3, 3);
					sums[y].setCaption(_.first(value));
					sums[y].setContent(_.last(value));
				    db2.addComponent(sums[y]);
				    y++;
				});

				db2interval = setInterval(function () {
		    		refreshPostGameStats();
		    	}, refreshTime2);

			});
			
			tdb.addDashboardTab(db2, {
	        	title: 'Post-Game Summary'
	    	});
	  	
        }, {tabbed: true});
	
    }
});

//$("#rfTabCore-1 > .rfDashboardCore").prepend('<div id="rfGroupHeader">'+topmode+'</h2>');

//console.log(pro.dbList[activeTab]);

function refreshLeaderBoard() {
	console.log(toporbottom);

	if(topmode == "Top Player"){
		toporbottom = bottomuserurl;
		topmode = "Bottom Player";
	}else{
		toporbottom = topuserurl;
		topmode = "Top Player";
	}
	//chart = chart;
	$.ajax({
	    type: "GET",
	    url: toporbottom,
	    success: function (newdata) {
	    	var x = 0;

        	db.setDashboardTitle ("Leaderboard: ");

	    	// empty chart arrays to repopulate with fresh data
    		chartlabels = [];
			chartskills = [];
			chartlucks = [];
			chartspeeds = [];
			charthands = [];

	    	$.each(newdata, function(key, value){
	    		// set last updated time
		        var t = moment().format("MM/DD/YYYY @ hh:mm:ss a");

				// create arrays for overall performance chart data
				chartlabels.push(value.Name);
				chartskills.push(Number(value.Skill));
				chartlucks.push(Number(value.Luck));
				chartspeeds.push(Number(moment.duration(value.LastTime).asMinutes()));
				charthands.push(Number(value.Hands));

				playerData = { 
					'Name' : value.Name,
					'Balance' : value.Balance,
					'Skill' : value.Skill,
					'Luck' : value.Luck,
					'Hands' : value.Hands,
					'Personality' : value.Personality,
					'Level' : value.Level,
					'Badges' : '',
					'StartTime' : value.StartTime,
					'LastTime' : value.LastTime,
					'DecisionTime' : moment.duration(value.LastTime).humanize(),
					'Updated' : t
				 };
				if(x <= maxplayers-1){ 
					//comps[x] = new PlayerComponent();
					//comps[x].setDimensions(3, 3);
					//console.log("x = " + x);
					//comps[x].lock();
					comps[x].clearData();
					comps[x].setCaption(topmode + " " + (x+1));
					comps[x].updatePlayer(playerData);
					//comps[x].unlock();
				    //db.addComponent(comps[x]);
    			}

    			x++;
    		});

			//console.log("db components: ");
			//console.log(db.components);

    		// chart for user performance
	    	chart.lock();
	    	chart.clearChart();
			chart.setDimensions (12, 6);
			if(topmode == "Top Player"){
				tmpmode = "All Player";
			}else{
				tmpmode = "Bottom " + maxplayers + " Player";
			}
			chart.setCaption("Realtime Performance for "+tmpmode+"s | Updated: " + moment().format("MM/DD/YYYY @ hh:mm:ss a"));
			chart.setYAxis("Skill/Luck/Speed");
	    	chart.setLabels (chartlabels);
			chart.addSeries ("Skill", "Skill", chartskills);
			chart.addSeries ("Luck", "Luck", chartlucks);
			chart.addSeries ("Hands", "Hands", charthands);
			chart.unlock();

    	}
    });
}

function refreshPostGameStats() {
	// Dashboard 2
	var sumbalance2 = $.getJSON(sumbalanceurl);
	var sumskills2 = $.getJSON(sumskillurl);
	var sumluck2 = $.getJSON(sumluckurl);
	var sumhands2 = $.getJSON(sumhandsurl);

	$.when(sumbalance2, sumskills2, sumluck2, sumhands2).done(function(balance, skill, luck, hands){

		var t3 = moment().format("MM/DD/YYYY @ hh:mm:ss a");
		
		// prepare all of the data to be dumped into an object we can loop over
    	var balancedata = _.values(JSON.parse(balance[2].responseText));
	    	var highbalance = _.last(balancedata);
	    	var lowbalance = _.first(balancedata);

	    	var skilldata = _.values(JSON.parse(skill[2].responseText));
	    	var highskill = _.last(skilldata);
	    	var lowskill = _.first(skilldata);

	    	var luckdata = _.values(JSON.parse(luck[2].responseText));
	    	var highluck = _.last(luckdata);
	    	var lowluck = _.first(luckdata);

	    	var handsdata = _.values(JSON.parse(hands[2].responseText));
	    	var highhands = _.last(handsdata);
	    	var lowhands = _.first(handsdata);

    	console.log(_.values(highbalance));

    	// dump into obect we can loop over
    	var postgamedataup = {
    		hbalance : [
    			'High Balance',
    			'<i class=\"fa fa-money fa-5x fa-highcolor\"></i><h2>'+_.values(highbalance)[0]+'</h2><p>$'+_.values(highbalance)[1]+'</p><date>Last Updated: '+t3+'</date>'
    		],
    		lbalance : [
    			'Low Balance',
    			'<i class=\"fa fa-money fa-5x fa-lowcolor\"></i><h2>'+_.values(lowbalance)[0]+'</h2><p>$'+_.values(lowbalance)[1]+'</p><date>Last Updated: '+t3+'</date>'
    		],
    		hskill : [
    			'High Skill',
    			'<i class=\"fa fa-trophy fa-5x fa-highcolor\"></i><h2>'+_.values(highskill)[0]+'</h2><p>'+_.values(highskill)[1]+'/200</p><date>Last Updated: '+t3+'</date>'
    		],
    		lskill : [
    			'Low Skill',
    			'<i class=\"fa fa-trophy fa-5x fa-lowcolor\"></i><h2>'+_.values(lowskill)[0]+'</h2><p>'+_.values(lowskill)[1]+'/200</p><date>Last Updated: '+t3+'</date>'
    		],
    		hluck : [
    			'High Luck',
    			'<i class=\"fa fa-star fa-5x fa-highcolor\"></i><h2>'+_.values(highluck)[0]+'</h2><p>'+_.values(highluck)[1]+'/200</p><date>Last Updated: '+t3+'</date>'
    		],
    		lluck : [
    			'Low Luck',
    			'<i class=\"fa fa-star fa-5x fa-lowcolor\"></i><h2>'+_.values(lowluck)[0]+'</h2><p>'+_.values(lowluck)[1]+'/200</p><date>Last Updated: '+t3+'</date>'
    		],
    		hhands : [
    			'High Hands',
    			'<i class=\"fa fa-hand-stop-o fa-5x fa-highcolor\"></i><h2>'+_.values(highhands)[0]+'</h2><p>'+_.values(highhands)[1]+' HANDS</p><date>Last Updated: '+t3+'</date>'
    		],
    		lhands : [
    			'Low Hands',
    			'<i class=\"fa fa-hand-stop-o fa-5x fa-lowcolor\"></i><h2>'+_.values(lowhands)[0]+'</h2><p>'+_.values(lowhands)[1]+' HANDS</p><date>Last Updated: '+t3+'</date>'
    		]
    	}

    	var z = 0;
    	$.each(postgamedataup, function(key, value){
    		console.log('looping through post game refresh modules:');
    		console.log(value);
			//sums[z].setCaption(_.first(value));
			//sums[z].lock();
			sums[z].updateContent(_.last(value));
			//sums[z].unlock();
			z++;
		});

	});
}

function tabRestart(active){
	console.log('tabRestart for '+active);
	if(active == "rfTabCore-1"){
		clearInterval(db1interval);
		clearInterval(db2interval);
		db1interval = setInterval(function () {
			refreshLeaderBoard()
    	}, refreshTime);
	}else if(active == "rfTabCore-2"){
		clearInterval(db1interval);
		clearInterval(db2interval);
		db2interval = setInterval(function () {
    		refreshPostGameStats();
    	}, refreshTime2);
	}
}

window.tabRestart = tabRestart;

